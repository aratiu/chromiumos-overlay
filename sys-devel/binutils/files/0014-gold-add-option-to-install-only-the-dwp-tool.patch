From 69d663d2e2f7f161d09860f622cfe96271eaec1d Mon Sep 17 00:00:00 2001
From: Adrian Ratiu <adrian.ratiu@collabora.com>
Date: Sat, 9 Jan 2021 04:06:08 +0200
Subject: [PATCH 14/14] gold: add option to install only the dwp tool

Distros/build-systems (ChromiumOS/portage, Yocto/bitbake...)
can have buggy recipes which pick a wrong linker, upstream
projects can hardcode or default to the wrong linker and other
bugs like these make it prudent to have gold disabled if not
necessary, hence the very useful existing --disable-gold flag.

However there are cases where dwp is needed even though gold
is not, so an option to enable only dwp in gold builds can be
helpful as well to avoid project specific hacks like removing
the gold linker in do_install() scripts just to have dwp.
---
 configure         |  4 +++-
 configure.ac      |  4 +++-
 gold/Makefile.am  |  2 ++
 gold/Makefile.in  |  3 +++
 gold/configure    |  4 ++++
 gold/configure.ac | 10 ++++++++--
 ld/configure      |  2 +-
 ld/configure.ac   |  4 ++--
 8 files changed, 26 insertions(+), 7 deletions(-)

diff --git a/configure b/configure
index 54d0339ab9e..b381d8c875a 100755
--- a/configure
+++ b/configure
@@ -3036,6 +3036,8 @@ fi
 #     Build both gold (installed as "ld.gold") and ld (installed as "ld"
 #     and ld.bfd).
 #     In other words, ld is default
+# --enable-gold=dwp
+#     Install only the DWP tool part of gold, not the linker itself
 # --enable-gold=default --enable-ld=default
 #     Error.
 
@@ -3048,7 +3050,7 @@ else
 fi
 
 case "${ENABLE_GOLD}" in
-  yes|default)
+  yes|default|dwp)
     # Check for ELF target.
     is_elf=no
     case "${target}" in
diff --git a/configure.ac b/configure.ac
index a910c4fd6ba..28e64e8e856 100644
--- a/configure.ac
+++ b/configure.ac
@@ -319,6 +319,8 @@ AC_ARG_ENABLE(offload-targets,
 #     Build both gold (installed as "ld.gold") and ld (installed as "ld"
 #     and ld.bfd).
 #     In other words, ld is default
+# --enable-gold=dwp
+#     Install only the DWP tool part of gold, not the linker itself
 # --enable-gold=default --enable-ld=default
 #     Error.
 
@@ -329,7 +331,7 @@ AC_ARG_ENABLE(gold,
 ENABLE_GOLD=$enableval,
 ENABLE_GOLD=no)
 case "${ENABLE_GOLD}" in
-  yes|default)
+  yes|default|dwp)
     # Check for ELF target.
     is_elf=no
     case "${target}" in
diff --git a/gold/Makefile.am b/gold/Makefile.am
index 3860e798106..a9758b46425 100644
--- a/gold/Makefile.am
+++ b/gold/Makefile.am
@@ -223,6 +223,7 @@ check: libgold.a
 
 install-exec-local: ld-new$(EXEEXT)
 	$(mkinstalldirs) $(DESTDIR)$(bindir) $(DESTDIR)$(tooldir)/bin
+	if test "x$(install_only_dwp)" != "xyes"; then \
 	n=`echo $(installed_linker) | sed '$(transform)'`; \
 	$(INSTALL_PROGRAM_ENV) $(INSTALL_PROGRAM) \
 	  ld-new$(EXEEXT) $(DESTDIR)$(bindir)/$${n}$(EXEEXT); \
@@ -241,6 +242,7 @@ install-exec-local: ld-new$(EXEEXT)
 	    ln $(DESTDIR)$(bindir)/$${n}$(EXEEXT) $(DESTDIR)$(tooldir)/bin/ld$(EXEEXT) >/dev/null 2>/dev/null \
 	    || $(INSTALL_PROGRAM_ENV) $(INSTALL_PROGRAM) ld-new$(EXEEXT) $(DESTDIR)$(tooldir)/bin/ld$(EXEEXT); \
 	  fi; \
+	fi; \
 	fi
 
 # We want install to imply install-info as per GNU standards, despite
diff --git a/gold/Makefile.in b/gold/Makefile.in
index f2906ede8b6..c35a5e94f26 100644
--- a/gold/Makefile.in
+++ b/gold/Makefile.in
@@ -628,6 +628,7 @@ htmldir = @htmldir@
 includedir = @includedir@
 infodir = @infodir@
 install_as_default = @install_as_default@
+install_only_dwp = @install_only_dwp@
 install_sh = @install_sh@
 installed_linker = @installed_linker@
 libdir = @libdir@
@@ -1583,6 +1584,7 @@ check: libgold.a
 
 install-exec-local: ld-new$(EXEEXT)
 	$(mkinstalldirs) $(DESTDIR)$(bindir) $(DESTDIR)$(tooldir)/bin
+	if test "x$(install_only_dwp)" != "xyes"; then \
 	n=`echo $(installed_linker) | sed '$(transform)'`; \
 	$(INSTALL_PROGRAM_ENV) $(INSTALL_PROGRAM) \
 	  ld-new$(EXEEXT) $(DESTDIR)$(bindir)/$${n}$(EXEEXT); \
@@ -1601,6 +1603,7 @@ install-exec-local: ld-new$(EXEEXT)
 	    ln $(DESTDIR)$(bindir)/$${n}$(EXEEXT) $(DESTDIR)$(tooldir)/bin/ld$(EXEEXT) >/dev/null 2>/dev/null \
 	    || $(INSTALL_PROGRAM_ENV) $(INSTALL_PROGRAM) ld-new$(EXEEXT) $(DESTDIR)$(tooldir)/bin/ld$(EXEEXT); \
 	  fi; \
+	fi; \
 	fi
 
 # We want install to imply install-info as per GNU standards, despite
diff --git a/gold/configure b/gold/configure
index 199a739e7d1..e30facb8f67 100755
--- a/gold/configure
+++ b/gold/configure
@@ -731,6 +731,7 @@ PLUGINS_FALSE
 PLUGINS_TRUE
 installed_linker
 install_as_default
+install_only_dwp
 EGREP
 GREP
 CPP
@@ -4958,6 +4959,9 @@ if test "${enable_gold+set}" = set; then :
      install_as_default=yes
    fi
    ;;
+ dwp)
+   install_only_dwp=yes
+   ;;
  esac
 else
   install_as_default=no
diff --git a/gold/configure.ac b/gold/configure.ac
index eca4f01c111..7b3fa05b32c 100644
--- a/gold/configure.ac
+++ b/gold/configure.ac
@@ -67,10 +67,12 @@ AC_DEFINE_UNQUOTED(TARGET_SYSTEM_ROOT_RELOCATABLE, $sysroot_relocatable,
 dnl "install_as_default" is true if the linker to be installed as the
 dnl default linker, ld.
 dnl "installed_linker" is the installed gold linker name.
+dnl "install_only_dwp" is true if only the DWP tool will be installed,
+dnl not the gold linker itself.
 
 installed_linker=ld.gold
 AC_ARG_ENABLE(gold,
-[[  --enable-gold[=ARG]     build gold [ARG={default,yes,no}]]],
+[[  --enable-gold[=ARG]     build gold [ARG={default,dwp,yes,no}]]],
 [case "${enableval}" in
  default)
    install_as_default=yes
@@ -80,10 +82,14 @@ AC_ARG_ENABLE(gold,
      install_as_default=yes
    fi
    ;;
- esac],
+ dwp)
+   install_only_dwp=yes
+   ;;
+esac],
 [install_as_default=no])
 AC_SUBST(install_as_default)
 AC_SUBST(installed_linker)
+AC_SUBST(install_only_dwp)
 
 AC_PLUGINS
 if test "$plugins" = "yes"; then
diff --git a/ld/configure b/ld/configure
index e20f53c414d..bf6877ddd52 100755
--- a/ld/configure
+++ b/ld/configure
@@ -15803,7 +15803,7 @@ if test "${enable_gold+set}" = set; then :
    install_as_default=no
    installed_linker=ld.bfd
    ;;
- yes|no)
+ yes|no|dwp)
    install_as_default=yes
    installed_linker=ld.bfd
    ;;
diff --git a/ld/configure.ac b/ld/configure.ac
index b77f8510ca6..a25b903fda5 100644
--- a/ld/configure.ac
+++ b/ld/configure.ac
@@ -108,13 +108,13 @@ dnl Use --enable-gold to decide if this linker should be the default.
 dnl "install_as_default" is set to false if gold is the default linker.
 dnl "installed_linker" is the installed BFD linker name.
 AC_ARG_ENABLE(gold,
-[[  --enable-gold[=ARG]     build gold [ARG={default,yes,no}]]],
+[[  --enable-gold[=ARG]     build gold [ARG={default,dwp,yes,no}]]],
 [case "${enableval}" in
  default)
    install_as_default=no
    installed_linker=ld.bfd
    ;;
- yes|no)
+ dwp|yes|no)
    install_as_default=yes
    installed_linker=ld.bfd
    ;;
-- 
2.31.1

